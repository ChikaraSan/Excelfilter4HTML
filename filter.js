//Based on https://stackoverflow.com/questions/1454655/onclick-event-for-whole-page-except-a-div
document.onclick = function () {
	if (clickedOutsideElement('myDropdown')) {
		var row = document.getElementById("tbtest").rows[0];
		var columns = row.cells.length;
		for (var i = 0; i < columns; i++) {
			if (document.getElementById("myDropdown" + i).classList.toggle("show") == true) {

				document.getElementById("myDropdown" + i).classList.toggle("show");
			}
		}
	}
}
//Based on https://stackoverflow.com/questions/1454655/onclick-event-for-whole-page-except-a-div
function clickedOutsideElement(elemId) {
	var theElem = getEventTarget(window.event);

	while (theElem = theElem.offsetParent) {
		if (theElem.id.includes(elemId))
			return false;
	}

	return true;
}
//Based on https://stackoverflow.com/questions/1454655/onclick-event-for-whole-page-except-a-div
function getEventTarget(evt) {
	var targ = (evt.target) ? evt.target : evt.srcElement;

	if (targ && targ.nodeType == 3)
		targ = targ.parentNode;

	return targ;
}

window.onload = function () {
	refEle(true);
};

function refEle(newValue) {
	var iStart = 0;
	for (var i = iStart; document.getElementById("tbtest").rows[iStart].cells.length > i; i++) {
		document.getElementById("myDropdown" + i).innerHTML = "";
		addEle(i, newValue);
	}
}

function myFunction(column) {
	document.getElementById("myDropdown" + column).classList.toggle("show");
}

//Based on https://www.w3schools.com/howto/howto_js_filter_table.asp
function filterFunction(column) {
	var input, filter, ul, li, a, i;
	input = document.getElementById("myInput" + column);
	filter = input.value.toUpperCase();
	div = document.getElementById("myDropdown" + column);
	a = div.getElementsByTagName("a");
	for (i = 2; i < a.length; i++) {
		txtValue = a[i].textContent || a[i].innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			a[i].style.display = "";
		} else {
			a[i].style.display = "none";
		}
	}
}

function addEle(column, newValue) {
	var t = document.getElementById("tbtest");
	var i = 0;
	var element = document.getElementById("myDropdown" + column);

	var tagAZ = document.createElement("a");
	var btnAZ = document.createElement("button");
	btnAZ.textContent = "A -> Z";
	btnAZ.setAttribute("onclick", "sortTable(" + column + ");");
	btnAZ.setAttribute("id", "btnAZ" + column);
	tagAZ.appendChild(btnAZ);
	element.appendChild(tagAZ);

	var breaker = document.createElement("hr");
	element.appendChild(breaker);

	var tagAll = document.createElement("a");
	var cbAll = document.createElement("input");
	cbAll.setAttribute("id", column + "-All");
	cbAll.setAttribute("type", "checkbox");
	if (newValue) {
		cbAll.setAttribute("checked", "");
	}
	cbAll.setAttribute("onclick", "checkAll(" + column + ");");
	tagAll.appendChild(cbAll);
	var text = document.createTextNode("Alle");
	tagAll.appendChild(text);
	element.appendChild(tagAll);

	var breaker = document.createElement("hr");
	element.appendChild(breaker);

	var values = [];
	var valCheck = [];
	var boxXi = [];

	var counter = 0;

	for (var i = 1, row; row = t.rows[i]; i++) {
		if (!values.includes(row.cells[column].innerText))
		{
			boxXi[row.cells[column].innerText] = i;
			values[counter] = row.cells[column].innerText;
			counter++;

			var tag = document.createElement("a");

			var cb = document.createElement("input");
			cb.setAttribute("id", column + "-" + i);
			cb.setAttribute("type", "checkbox");
			cb.setAttribute("indeterminate", "false");

			valCheck[row.cells[column].innerText] = row.style.display;
			if (row.style.display != "none")
				cb.setAttribute("checked", "");

			cb.setAttribute("onclick", "Hide(" + column + ",'" + row.cells[column].innerText + "','" + i +"');");
			tag.appendChild(cb);
			var text = document.createTextNode(/*" -> " +*/ row.cells[column].innerText);
			tag.appendChild(text);
			element.appendChild(tag);
		}
		else {
			if (valCheck[row.cells[column].innerText] != row.style.display) {
				var checkboxi = document.getElementById(column + "-" + boxXi[row.cells[column].innerText]);
				checkboxi.indeterminate = true;
            }
        }
	}

	var breaker = document.createElement("hr");
	element.appendChild(breaker);
}

function Hide(columNR, cellv, i) {

	var checkboxi = document.getElementById(columNR + "-" + i);
	var rows = document.getElementById("tbtest").rows;

	for (var i = 1; i < rows.length; i++) {
		var row = rows[i];

		if (row.cells[columNR].innerText == cellv) {
			if (checkboxi.checked) {
				row.style.display = "";
			}
			else {
				row.style.display = "none";
            }
		}
		
	}
	refEle(true);
}

function checkAll(column) {
	var element = document.getElementById("myDropdown" + column);
	var checkboxesAll = element.querySelectorAll('input[type="checkbox"]');
	var val = document.getElementById(column + "-All").checked;

	//alert(checkboxesAll.length);

	for (var i = 0; i < checkboxesAll.length; i++) {
		checkboxesAll[i].checked = val;
	}

	var rows = document.getElementById("tbtest").rows;
	for (var i = 1; i < rows.length; i++) {
		if (val) {
			rows[i].style.display = "";
		}
		else {
			rows[i].style.display = "none";
		}
	}

	refEle(val);
}
//Based on https://www.w3schools.com/howto/howto_js_sort_table.asp
function sortTable(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

	var btn = document.getElementById("btnAZ" + n);
	if (btn.firstChild.data == "A -> Z") {
		btn.firstChild.data = "Z -> A";
	}
	else {
		btn.firstChild.data = "A -> Z";
    }

	table = document.getElementById("tbtest");
	switching = true;
	// Set the sorting direction to ascending:
	dir = "asc";
	/* Make a loop that will continue until
	no switching has been done: */
	while (switching) {
		// Start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/* Loop through all table rows (except the
		first, which contains table headers): */
		for (i = 1; i < (rows.length - 1); i++) {
			// Start by saying there should be no switching:
			shouldSwitch = false;
			/* Get the two elements you want to compare,
			one from current row and one from the next: */
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];

			var numx = Math.fround(parseFloat(x.innerHTML.toLowerCase()).toFixed(2));
			var numy = Math.fround(parseFloat(y.innerHTML.toLowerCase()).toFixed(2));

			if ((!isNaN(numx) && !isNaN(numy))) {
				if (dir == "asc") {
					if (numx > numy) {
						shouldSwitch = true;
						break;
					}
				} else if (dir == "desc") {
					if (numx < numy) {
						shouldSwitch = true;
						break;
					}
				}
			}
			else {
				/* Check if the two rows should switch place,
				based on the direction, asc or desc: */
				if (dir == "asc") {
					if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
						// If so, mark as a switch and break the loop:
						shouldSwitch = true;
						break;
					}
				} else if (dir == "desc") {
					if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
						// If so, mark as a switch and break the loop:
						shouldSwitch = true;
						break;
					}
				}
			}
		}
		if (shouldSwitch) {
			/* If a switch has been marked, make the switch
			and mark that a switch has been done: */
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			// Each time a switch is done, increase this count by 1:
			switchcount++;
		} else {
			/* If no switching has been done AND the direction is "asc",
			set the direction to "desc" and run the while loop again. */
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
	document.getElementById("myDropdown" + n).classList.toggle("show");
}